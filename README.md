# Config Reborn!

Semi automatic provisioning after an Arch Linux install!

## Installation Instructions
1. Install ansible `sudo pacman -S ansible`
6. Run ansible playbook. `ansible-playbook main.yml --ask-become-pass` to install everything, or `ansible-playbook main.yml --ask-become-pass --tag "bluetooth"` to install a specific tag, replacing theming with whatever catches your fancy.  If you dont want to be prompted for an email you can do: `ansible-playbook main.yml --ask-become-pass -e email_address=<imap email>`
<!-- 7. Open up nvim, run `:PackerSync` `:COQdeps` -->


## Manual Steps

### Hardware

#### Cups

Go into System Settings/Printers and add your connected printer. Occasionally it won't let you add it, in that case go to [http://localhost:631/admin](http://localhost:631/admin) and add it there.

The settings for a *Zebra LP2844-Z* are:

Media Size: `4.00x6.00"`
Resolution: `203dpi`
Media Tracking: `Non-continuous (Web sensing)`
Media Type: `Direct Thermal Media`


### KDE Connect

Open and pair devices.

### Syncthing

Open ([http://127.0.0.1:8384/](http://127.0.0.1:8384/)) and pair devices/folders.
