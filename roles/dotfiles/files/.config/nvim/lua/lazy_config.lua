-- Bootstrap lazy
local lazypath = vim.fn.stdpath("data") .. "/lazy/lazy.nvim"
if not vim.loop.fs_stat(lazypath) then
  vim.fn.system({
    "git",
    "clone",
    "--filter=blob:none",
    "https://github.com/folke/lazy.nvim.git",
    "--branch=stable", -- latest stable release
    lazypath,
  })
end
vim.opt.rtp:prepend(lazypath)

vim.g.mapleader = " "       -- Make sure to set `mapleader` before lazy so your mappings are correct
vim.g.maplocalleader = "\\" -- Same for `maplocalleader`


-- Plugins
require("lazy").setup({
  {
    -- Gruvbox theme
    "sainnhe/gruvbox-material",
    config = function()
      vim.o.background = "dark"
      vim.o.termguicolors = true
      vim.g.gruvbox_material_background = 'hard'
      vim.g.gruvbox_material_better_performance = 1
      vim.cmd [[colorscheme gruvbox-material"]]
    end
  },
  "folke/twilight.nvim", -- Dim paragraphs not being worked on
  {
    -- Distraction free coding
    "folke/zen-mode.nvim",
    opts = {
      window = {
        options = {
          number = false,
          list = false,
          relativenumber = false,
          cursorcolumn = false
        }
      }
    }
  },
  "folke/neodev.nvim", -- Auto configure lua-language-server
  {
    -- Autocomplete brackets and the like
    'windwp/nvim-autopairs',
    event = "InsertEnter",
    opts = {} -- this is equalent to setup({}) function
  },
  {
    -- Preview lines without jumping to the line
    "nacro90/numb.nvim",
    config = function()
      require("numb").setup()
    end
  },
  {
    -- Colorise hex codes e.g. #008080
    "NvChad/nvim-colorizer.lua",
    config = function()
      require("colorizer").setup()
    end
  },
  {
    -- Show git info
    "lewis6991/gitsigns.nvim",
    config = function()
      require("gitsigns").setup()
    end
  },
  {
    -- Floow MD links
    'jghauser/follow-md-links.nvim',
    config = function()
      vim.keymap.set('n', '<bs>', ':edit #<cr>', { silent = true }) -- use backspace to return to the previous file
    end
  },
  {
    -- Markdown
    'MeanderingProgrammer/markdown.nvim',
    name = 'render-markdown', -- Only needed if you have another plugin named markdown.nvim
    dependencies = { 'nvim-treesitter/nvim-treesitter' },
    config = function()
      require('render-markdown').setup({})
    end
  },
  {
    "nvim-tree/nvim-tree.lua",
    config = function()
      require("nvim-tree").setup()
    end
  },
  {
    -- Code completion and snippets
    "hrsh7th/nvim-cmp",
    dependencies = {
      'L3MON4D3/LuaSnip',
      'saadparwaiz1/cmp_luasnip',
      "hrsh7th/cmp-nvim-lsp",
      'petertriho/cmp-git',
      "/hrsh7th/cmp-buffer",
      "hrsh7th/cmp-cmdline",
      "lukas-reineke/cmp-rg",
      'roginfarrer/cmp-css-variables',
      "https://codeberg.org/FelipeLema/cmp-async-path",
      {
        'petertriho/cmp-git',
        dependencies = {
          "nvim-lua/plenary.nvim"
        },
        config = function()
          require("cmp_git").setup({})
        end
      },
    }
  },
  {
    -- Mason for LSP
    "williamboman/mason.nvim",
    dependencies = {
      "neovim/nvim-lspconfig",
    },
    config = function()
      require("mason").setup()
    end
  },
  {
    "numToStr/Comment.nvim",
    config = function()
      require('Comment').setup()
    end
  },
  {
    'nvim-telescope/telescope-fzf-native.nvim',
    build =
    'cmake -S. -Bbuild -DCMAKE_BUILD_TYPE=Release && cmake --build build --config Release && cmake --install build --prefix build'
  },
  {
    "ibhagwan/fzf-lua",
    -- optional for icon support
    dependencies = { "nvim-tree/nvim-web-devicons" },
    config = function()
      -- calling `setup` is optional for customization
      require("fzf-lua").setup({})
    end
  },
  {
    'nvim-telescope/telescope.nvim',
    dependencies = {
      'nvim-lua/plenary.nvim',
    }
  },
  {
    "mfussenegger/nvim-lint",
    config = function()
      require('lint').linters_by_ft = {
        dockerfile = {
          "hadolint",
          'trivy',
        },
        lua = {
          'selene',
          "luacheck",
        },
        json = {
          "jsonlint"
        },
        bash = {
          "shellharden",
        },
        go = {
          'trivy',
        },
        markdown = {
          'proselint',
          'vale',
          'alex',
        },
        htmlhint = {
          "htmlhint"
        },
        python = {
          "flake8",
          'trivy',
        },
        text = {
          'proselint',
          'vale',
        },
        javascript = {
          "eslint_d"
        },
        typescript = {
          "eslint_d"
        },
        tf = {
          "tflint",
          'tfsec',
          'trivy',
        },
        yaml = {
          "actionlint",
          "yamllint",
        }
      }
    end
  },
  "mfussenegger/nvim-dap",
  {
    'stevearc/conform.nvim',
    event = {
      "BufReadPre",
      "BufNewFile"
    },
    config = function()
      require("conform").setup({
        formatters_by_ft = {
          python = {
            "reorder-python-imports",
            "black",
            "autoflake",
          },
          bash = {
            "beautysh",
            "shellharden",
          },
          go = {
            "djlint",
          },
          tf = {
            "terraform_fmt",
          },
          hcl = {
            "hclfmt",
          },
          css = {
            "prettierd",
          },
          html = {
            "prettierd",
          },
          lua = {
            "luaformatter",
          },
          sh = {
            "beautysh",
          },
          zsh = {
            "beautysh",
          },
          markdown = {
          },
          json = {
            "jq",
            "prettierd",
          },
          yaml = {
            "prettierd",
          }
        },
        format_on_save = {
          lsp_fallback = true,
          async = false,
          timeout_ms = 500
        }
      })
    end

  },
  {
    'chomosuke/typst-preview.nvim',
    ft = 'typst',
    version = '0.3.*',
    build = function() require 'typst-preview'.update() end,
  },
  {
    'WhoIsSethDaniel/mason-tool-installer.nvim',
    config = function()
      require('mason-tool-installer').setup({
        ensure_installed = {
          "actionlint",
          "arduino-language-server",
          "ansible-language-server",
          "autoflake",
          "bash-debug-adapter",
          "bash-language-server",
          "beautysh",
          "black",
          "clangd",
          "css-variables-language-server",
          "css-lsp",
          "debugpy",
          "docker-compose-language-service",
          "dockerfile-language-server",
          "eslint_d",
          "flake8",
          "typst-lsp",
          "luaformatter",
          "hclfmt",
          "gitlab-ci-ls",
          "shellharden",
          "gopls",
          "djlint",
          "hadolint",
          "html-lsp",
          "htmlhint",
          "prettierd",
          "jq-lsp",
          "js-debug-adapter",
          "json-lsp",
          "jsonlint",
          "lua-language-server",
          "luacheck",
          "marksman",
          "proselint",
          "pyright",
          "reorder-python-imports",
          "rust-analyzer",
          "selene",
          "terraform-ls",
          "tflint",
          "tfsec",
          "trivy",
          "vale",
          "vale-ls",
          "yaml-language-server",
          "yamllint",
        }
      })
    end
  }
})
