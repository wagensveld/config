-- Display
vim.g.encoding = "utf-8" -- Text Encoding
vim.o.spelllang = "en_au,cjk" -- Spelling
vim.o.spell = true -- Set Spell checker
vim.o.title = true -- Set terminal title
vim.o.background = "dark" -- For themes
vim.o.termguicolors = true -- Set terminal colours for colorizer
vim.o.titlestring = "nvim %t" -- Set terminal title to filename
vim.wo.number = true -- Absolute Numbers
vim.wo.relativenumber = true -- Relative Numbers
vim.wo.colorcolumn = "121" -- Visual marker for column 121
vim.wo.linebreak = true -- Line break
vim.wo.cursorcolumn = true -- Highlight the cursor column
vim.wo.cursorline = true -- Highlight the cursor line
vim.o.breakindent = true -- Break indent
vim.o.breakindentopt = "shift:2" -- Shift indent break indent by 2 spaces
vim.o.showbreak = "↪" -- On break indent use ↪
vim.opt.list = true -- Visualise spaces and the like
vim.opt.listchars:append("eol:↲") -- Display end of line as ↲
vim.opt.listchars:append("tab:→ ") -- Display tab as →
vim.opt.listchars:append("space:⋅") -- Show spaces as ⋅
vim.opt.listchars:append("trail:•") -- Show trailing space as •
vim.opt.listchars:append("nbsp:␣") -- Show non-breakable space as ␣
vim.o.foldlevelstart = 99 -- Don't fold on start
vim.o.lazyredraw = true -- Don't redraw screen in middle of macro (for speed)

-- Search
vim.o.ignorecase = true -- Ignore case when searching
vim.o.smartcase = true  -- Match 'the' to 'The' but not 'The' to 'the'\
vim.o.incsearch = true  -- Highlight search

-- Behaviour
vim.o.tabstop = 2       -- Set tab no. characters
vim.o.shiftwidth = 2    -- Set indentation no. characters
vim.o.softtabstop = 2   -- Number of characters during edit operations
vim.o.expandtab = true  -- Use spaces when using a <Tab>, to insert a Tab character: Do <C>-V<Tab>
vim.o.shiftround = true -- When shifting lines round to closest multiple of shiftwidth
vim.o.scrolloff = 999   -- Centre cursor

-- File System Stuff
vim.o.undodir = os.getenv("HOME") .. "/.local/share/nvim/undo/" -- Set Location for Undo File Directory
vim.o.undofile = true                                           -- Enable Undo Files
vim.o.clipboard = "unnamedplus"                                 -- Use System Clipboard
